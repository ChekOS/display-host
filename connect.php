<?php

include 'library.php';
include 'form.php';

$auth = array();
$query_auth = mysqli_query($c,
    'SELECT tablet_id, room_id
	FROM tablet_room');
while ($elem = mysqli_fetch_array($query_auth)) {
    $elem = clean_mysqli_array($elem);
    $auth[$elem['tablet_id']] = $elem['room_id'];
}

if (isset($_POST['tablet_id'])) {
    $info = remove_tags($_POST);
    if (isset($auth[$info['tablet_id']])) {
        $_SESSION['room_id'] = $auth[$info['tablet_id']];
        $forheader = 'Location: index.php';
        header($forheader);
        exit;
    } else {
        $error = true;
    }
}

$result = '<h1>Connect</h1>';

$result .= '<form action ="'.$_SERVER['PHP_SELF'].'" method="POST">';

$macElement = addTextElement(array(
    'name' => 'tablet_id',
    'id' => 'tablet_id',
    'title' => 'MAC address: ',
    'value' => $_SESSION['tablet_id'],
));

$result .= $macElement;

$result .= '<input type="submit" value="Connect"><br>';
$result .= '</form>';

if(isset($error)) {
    $result .= '<h2 style="color:red;">MAC address doesn`t match with any room! <a href="/bind.php">Go to Bind tablets</a></h2>';
}

echo $result;

?>