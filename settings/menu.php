<?php

if ($_SESSION['authorized']) {
    $role = 'admin';
} else {
    $role = 'user';
}

$MENU = array(
    'admin' => array(
        '/exit.php' => array(
            'title' => 'Log out',
            'icon' => '',
        ),
        '/connect.php' => array(
            'title' => 'Connect',
            'icon' => '',
        ),
        'bind/index.php' => array(
            'title' => 'Bind tablets',
            'icon' => '',
        )
    ),
    'user' => array(
        'auth.php' => array(
            'title' => 'Log in',
            'icon' => '',
        ),
    )
);

$result = '<h1>MENU</h1>';

foreach ($MENU[$role] as $link => $attr) {
    $result .= '<button type="button"><a href="' . $link . '">' . $attr['title'] . '</a></button><br><br>';
}

echo $result;

