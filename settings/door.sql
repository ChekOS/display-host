-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               8.0.27 - MySQL Community Server - GPL
-- Операционная система:         Win64
-- HeidiSQL Версия:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Дамп структуры для таблица door.building
CREATE TABLE IF NOT EXISTS `building` (
  `building_id` varchar(10) NOT NULL DEFAULT '',
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`building_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- Дамп данных таблицы door.building: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `building` DISABLE KEYS */;
INSERT INTO `building` (`building_id`, `name`) VALUES
	('00000', 'LUT'),
	('38096', 'Niemenkatu 73'),
	('46287', 'Mukkulankatu 19 A 1. krs'),
	('46288', 'Mukkulankatu 19 A 2. krs'),
	('46289', 'Mukkulankatu 19 B 2. krs'),
	('46290', 'Mukkulankatu 19 C 2. krs'),
	('46291', 'Mukkulankatu 19 D 2. krs'),
	('47658', 'Mukkulankatu 19 D 1. krs'),
	('47732', 'Ulkopuolinen tila'),
	('51493', 'Skinnarila AMK'),
	('53468', 'Skinnarila 1-vaihe'),
	('53469', 'Skinnarila 2-vaihe'),
	('53470', 'Skinnarila 3-vaihe'),
	('53471', 'Skinnarila 6-vaihe'),
	('53472', 'Skinnarila 7-vaihe'),
	('53473', 'Skinnarila YO-talo');
/*!40000 ALTER TABLE `building` ENABLE KEYS */;

-- Дамп структуры для таблица door.tablet_room
CREATE TABLE IF NOT EXISTS `tablet_room` (
  `tablet_room_id` int NOT NULL AUTO_INCREMENT,
  `tablet_id` varchar(50) DEFAULT NULL,
  `room_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`tablet_room_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;

-- Дамп данных таблицы door.tablet_room: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `tablet_room` DISABLE KEYS */;
INSERT INTO `tablet_room` (`tablet_room_id`, `tablet_id`, `room_id`) VALUES
	(1, '00-B0-D0-63-C2-26', '1370'),
	(2, '00-B0-D0-63-C2-25', NULL),
	(3, NULL, '1566');
/*!40000 ALTER TABLE `tablet_room` ENABLE KEYS */;

-- Дамп структуры для таблица door.user
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;

-- Дамп данных таблицы door.user: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`user_id`, `login`, `password`) VALUES
	(1, 'admin', '1a1dc91c907325c69271ddf0c944bc72');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
