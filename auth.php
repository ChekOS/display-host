<?php

include 'library.php';
include 'form.php';

$auth = array();
$query_auth = mysqli_query($c,
	'SELECT login, password
	FROM user');
while ($elem = mysqli_fetch_array($query_auth)) {
	$elem = clean_mysqli_array($elem);
	$auth[$elem['login']] = $elem['password'];
}

if (isset($_POST['login'])) {
	$info = remove_tags($_POST);
	if (isset($auth[$info['login']]) and $auth[$info['login']] == md5($info['password'])) {
		$_SESSION['authorized'] = true;
		$forheader = 'Location: ';
		if (isset($_SESSION['page'])) {
			$forheader .= $_SESSION['page'];
		} else {
			$forheader .= 'index.php';
		}
		$_SESSION['login'] = $info['login'];
		header($forheader);
		exit;
	} else {
		$error = true;
	}
}

$result = '
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>login</title>
    <link rel="stylesheet" href="public/css/style.css">

    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" type="text/css">
    

</head>
<body>

<form action ="'.$_SERVER['PHP_SELF'].'" method="POST">
<div class="form-box">
    <h1>Welcome</h1>
    

    <div class="input-box">
        <i class="fa fa-envelope-o"></i>
        <input type="text" name="login" placeholder="Login" id="login" required>
    </div>

    <div class="input-box">
        <i class="fa fa-key"></i>
        <input type="password" name="password" placeholder="Password" id="password" required>

        <span class="eye" onclick="myeye()">
                <i id="hide1" class="fa fa-eye"></i>
                <i id="hide2" class="fa fa-eye-slash"></i>
        </span>
    </div>

    <input type="submit" class="login_btn" onclick="saveAuth()" value="LOGIN">

</div>
</form>
</body>';

if(isset($error)) {
	$result .= '<h2 style="color:red;">Login or password are incorrect!</h2>';
}

echo $result;

?>

<script>

function saveAuth() {
	save_confirm = confirm('Would you like to save your authorization data?');
	if (save_confirm) {
		localStorage.setItem('login', document.getElementById("login").value);
		localStorage.setItem('password', document.getElementById("password").value);
	} else {
		localStorage.removeItem('login');
		localStorage.removeItem('password');
	}
}

        function myeye()
        {
            var x = document.getElementById("password");
            var y = document.getElementById("hide1");
            var z = document.getElementById("hide2");

            if(x.type === 'password')
            {
                x.type='text';
                y.style.display = "block";
                z.style.display = "none";

            }
            else{
                x.type='password';
                y.style.display = "none";
                z.style.display = "block";
            }



        }

if (localStorage.getItem('login') != null && localStorage.getItem('password') != null) {
	document.getElementById("login").value = localStorage.getItem('login');
	document.getElementById("password").value = localStorage.getItem('password');
}

</script>