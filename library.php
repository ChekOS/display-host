<?php

include 'settings/config.php';
session_start();
$c = mysqli_connect($DATABASE['host'], $DATABASE['username'], $DATABASE['password']);
mysqli_select_db($c, $DATABASE['dbname']);

if (!isset($_SESSION['authorized']) && !in_array($_SERVER['PHP_SELF'], array('/auth.php', '/index.php'))) {
	$_SESSION['page'] = $_SERVER['PHP_SELF'];
	header('Location: index.php');
	exit;
}

// вывод таблицы напрямую из SQL

function print_table($c, $query) {
	
	$query_info = mysqli_query($c, $query);
	
	// выводим заголовки
	$result = '<style> th, td {border-style: groove;}</style><table><tr>';
	$first_row = clean_mysqli_array(mysqli_fetch_array($query_info));	
	foreach ($first_row as $title => $value) {
		$result .= '<th>'.$title.'</th>';
	}
	$result .= '</tr>';
	
	// выводим первую строку
	$result .= '<tr>';	
	foreach ($first_row as $title => $value) {
		$result .= '<td>';
		if (isset($first_row[$title])) {			
			$result .= $value;
		}
		$result .= '</td>';
	}	
	$result .= '</tr>';	

	while($elem = mysqli_fetch_array($query_info)){	
		$elem = clean_mysqli_array($elem);
	
		$result .= '<tr>';
	
		foreach ($elem as $title => $value) {
			$result .= '<td>';
			if (isset($elem[$title])) {			
				$result .= $value;
			}
			$result .= '</td>';
		}		
		
		$result .= '</tr>';
	}
	
	$result .= '</table>';
	
	echo ($result);
}

// изменения

function db_insert($c, $info, $FIELDS, $tbl_name, $key) {
	
	$query_insert = 'INSERT	INTO '.$tbl_name.' (';
	foreach ($FIELDS as $name => $value) {
		if (isset($info[$name]) and $info[$name] != '' and $name != $key) {
			$query_insert .= $name.', ';			
		}
	}
	$query_insert = substr($query_insert, 0, -2);
	$query_insert .= ') VALUES (';
	
	foreach ($FIELDS as $name => $value) {
		if (isset($info[$name]) and $info[$name] != '' and $name != $key) {
				$query_insert.= '"'.$info[$name].'"';
				$query_insert .= ', ';
		}		
	}

	$query_insert = substr($query_insert, 0, -2);
	$query_insert .= ');';
	
	//echo $query_insert;
	mysqli_query($c, $query_insert);

}

function db_update($c, $info, $FIELDS, $tbl_name, $key) {
	if(isset($info)){
		$query_update = 'UPDATE '.$tbl_name.' SET ';
		foreach ($FIELDS as $name => $value) {
			if (isset($info[$name]) and $name != $key) {
				if ($info[$name] == '') {
					$query_update .= ' '.$name.' = NULL';
				} else {
					$query_update .= ' '.$name.' = "'.$info[$name].'"';
				}
				$query_update .= ', ';
				}				
			}
		}
	$query_update = substr($query_update, 0, -2);
	$query_update .= ' WHERE '.$key.' = '.$info[$key].';';
	
	//echo $query_update;
	mysqli_query($c, $query_update);
}

function db_delete($c, $fordelete, $tbl_name, $key) {
	
	$query_delete = 'DELETE FROM '.$tbl_name.' WHERE '.$key.' IN ('.implode(', ', $fordelete).');';
	
	//echo ($query_delete);
	mysqli_query($c, $query_delete);
	
}

// служебные функции

function compare_user($user_db, $user_file, $key) {
	
	$info = array();
	foreach ($user_file as $name => $value) {
		if ($value != $user_db[$name]) {
			$info[$name] = $value;
		}
	}
	if (!empty($info)) {
		$info[$key] = $user_db[$key];
	}
	//var_dump($info);
	return $info;
	
}

function clean_mysqli_array($array) {
	
	// для очиски массива от дубликатов
	$info = array();
	foreach($array as $title => $value) {
		if (is_string($title)) {
			$info[$title] = $value;
		}
	}
	return($info);
}

function remove_tags($arr) {	
	$info = array();
	foreach ($arr as $key => $value) {
		$info[$key] = htmlspecialchars($value);		
	}
	return $info;	
}

// работа с формой

function form_generator($FIELDS, $info, $c, $errors) {
	
	$result="";

	$result .= '<form method="post" action="'.$_SERVER['PHP_SELF'].'">';
	
	$result .= '<input type="hidden" name="mode" value="'.$info['mode'].'">';
	
	foreach ($FIELDS as $name => $attr) { //$attr = attribute
		$result .= '<h2>'.$attr['title'].':';
		if ($attr['required']) {
			$result .= '*';
		}
		
		switch ($attr['type']) {
			
			case 'hidden':
				if (isset($info[$name]) and $info['mode'] == 'update') {
					$value_for_id = $info[$name];
				} else {
					$value_for_id = ' Новый ID';
				}
				$result .= ' '.$value_for_id;
				$result .= '<input type="'.$attr['type'].'" name="'.$name.'" value="';
				$result .= $value_for_id;				
				$result .= '">';
				break;
				
			case 'text':
				$result .= ' <input type="'.$attr['type'].'" name="'.$name.'" value="';
				if (isset($info[$name])){
					$result .= $info[$name];
				}
				$result .= '">';
				break;
				
			case 'select':
				$result .= ' <'.$attr['type'].' name="'.$name.'">';
				$query_org = mysqli_query($c, $attr['data']);
				$result .= '<option value=""></option>';
				while($elem = mysqli_fetch_array($query_org)){	
					$result .= '<option value="'.$elem['org_id'].'"';
					if (isset($info[$name]) and $info[$name] == $elem['org_id']){
							$result .= ' selected="selected"';
						}
					$result .= '>'.$elem['name'].'</option>';
				}
				$result .= '</select>';
				break;
				
			case 'radio':
				$result .= '<br>';
				foreach ($attr['data'] as $k => $selection){
					$result .= '<input type="'.$attr['type'].'" name="'.$name.'" value="'.$k.'"';
					if (isset($info[$name]) and $info[$name] == $k){
							$result .= ' checked';
						}
					$result .= '>'.$selection.'<br>';
				}
				break;				
		}	
		
		$result .= '</h2>';
		if (isset($errors[$name])){
			$result .= $errors[$name];
		}		
	}
	
	$result .= '<input type="submit" name="submit" value="Submit">';	
	$result .= '</form>';
	echo $result;
}

function errorcheck($fields, $info) {
	$errors = array();
	foreach ($fields as $name => $attr)	{
		if ($attr['required'] and $info[$name] == '') {
			$errors[$name] = $attr['title'].' cannot be empty!<br>';
		}
	}
	return $errors;
}

function pr($array) {
    echo "<xmp>";
    if (!isset($array)) echo "pr(): value not set";
    elseif (is_object($array))  print_r($array);
    elseif (!is_array($array)) echo "pr() string: $array";
    elseif (!count($array)) echo "pr(): array empty";
    else print_r($array);
    echo "</xmp>";
}

?>