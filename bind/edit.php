<?php

include '../library.php';
include '../form.php';

$result = '<h1>Create</h1>';

$macElement = addTextElement(array(
    'name' => 'tablet_id',
    'id' => 'tablet_id',
    'title' => 'MAC address: ',
    'value' => $_SESSION['tablet_id'],
));

$result .= $macElement;

$postfields = array('building' => '00000');

$url = "https://tilat.lab.fi/getRooms.php?building=00000";
//$url = 'https://www.example.com';
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HEADER, false);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($curl, CURLOPT_POST, false);

$json = curl_exec($curl);
curl_close($curl);

$rooms = json_decode($json, true);
$rooms = array_flip($rooms);

$result .= addSelectElement(array(
    'name' => 'rooms',
    'id' => 'rooms',
    'title'
));'<select name="rooms" id="rooms">';
foreach ($rooms as $id => $room) {
    $result .= '<option value="' . $id .'">' . $room . '</option>';
}
$result .= '</select>';

echo $result;





