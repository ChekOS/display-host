<?php

function addTextElement($props) {
    $element = '<label for="' . $props['name'] . '">' . $props['title'] . '</label>';
    $element .= '<input type="text" ';
    $element .= 'name="'. $props['name'] . '" ';
    if ($props['id']) $element .= 'id="'. $props['id'] . '" ';
    if ($props['placeholder']) $element .= 'placeholder="'. $props['placeholder'] . '" ';
    if ($props['value']) $element .= 'value="'. $props['value'] . '" ';
    $element .= '>';
    return $element;
}

function addPasswordElement($props) {
    $element = '<label for="' . $props['name'] . '">' . $props['title'] . '</label>';
    $element .= '<input type="password" ';
    $element .= 'name="'. $props['name'] . '" ';
    $element .= 'id="'. $props['id'] . '" ';
    $element .= 'value="'. $props['value'] . '" ';
    $element .= '>';
    return $element;
}

function addSelectElement($props) {
    $element = '<label for="' . $props['name'] . '">' . $props['title'] . '</label>';
    $element .= '<select name="' . $props['name'] . '" id="' . $props['id'] . '">';
    foreach ($props['options'] as $id => $option) {
        $element .= '<option value="' . $id .'">' . $option . '</option>';
    }
    $element .= '</select>';
    return $element;
}

?>